from flask import Flask, g
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager, current_user
from flask.ext.migrate import Migrate
from flask.ext.bcrypt import Bcrypt
from flask.ext.restless import APIManager
from config import DevConfig


app = Flask(__name__)
app.config.from_object(DevConfig)

lm = LoginManager(app)
lm.login_view = 'login'

bcrypt = Bcrypt(app)

db = SQLAlchemy(app)
api = APIManager(app, flask_sqlalchemy_db=db)

migrate = Migrate(app, db)

from  entries.blueprint import entries
app.register_blueprint(entries, url_prefix='/entries')

@app.before_request
def before_request():
    g.user = current_user

import admin





















































