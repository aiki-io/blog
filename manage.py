#!/usr/bin/env python3
from  flask.ext.script import Manager, Server
from flask.ext.migrate import MigrateCommand
from app import app, db
import api
import models
import views

manager = Manager(app)

manager.add_command('db', MigrateCommand)

@manager.shell
def make_shell_context():
    return dict(app=app, db=db)

@manager.command
def createdb():
    'create database'
    db.create_all()




if __name__=='__main__':
    manager.run()
