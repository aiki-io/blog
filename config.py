import os

class Config:
    APP_DIR = os.path.dirname(os.path.realpath(__file__))
    STATIC_DIR = os.path.join(APP_DIR, 'static')
    IMAGES_DIR = os.path.join(STATIC_DIR, 'images')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class DevConfig(Config):
    DEBUG=True
    SECRET_KEY='flask is fun!'
    #SQLALCHEMY_DATABASE_URI='sqlite:///{}/blog.db'.format(Config.APP_DIR)
    SQLALCHEMY_DATABASE_URI='postgresql://localhost/blog'
